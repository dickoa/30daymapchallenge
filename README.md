# 30DayMapChallenge 2020

*A list of maps I created for #30DayMapChallenge using #rspatial*.
To learn more about #30DayMapChallenge visit https://github.com/tjukanovt/30DayMapChallenge.

## Day 1

2020-11-01 - Points - *A map with points*

<img src="figures/day1_point.png" width="500">

## Day 2

2020-11-02 - Lines - *A map with lines*

<img src="figures/day2_line.png" width="500">

## Day 3

2020-11-03 - Polygons - *A map with polygons*

<img src="figures/day3_poly.png" width="500">

## Day 4

2020-11-04 - Hexagons - *A map with hexagons*

<img src="figures/day4_hex.png" width="500">

## Day 5

2020-11-05 - Blue - *A map with blue colour or a map about something blue*

<img src="figures/day5_blue.png" width="500">

## Day 6

2020-11-06 - Red - *A map with red colour or a map about something red*

<img src="figures/day6_red.png" width="500">

## Day 7

2020-11-07 - Green - *A map with green colour or a map about something green*

<img src="figures/day7_green.png" width="500">

## Day 8

2020-11-08 - Yellow - *A map with yellow colour or a map about something yellow*


<img src="figures/day8_yellow.png" width="500">

## Day 9

2020-11-09 - Monochrome - *A monochromic image is composed of one color (or values of one color). So for example black and white maps are valid here. [See some inspiration from the great monochrome mapping competition](https://somethingaboutmaps.wordpress.com/monocarto-2019-winners/) (although those masterpieces took a bit more than a day to make, probably).*

<img src="figures/day9_monochrome.png" width="500">


## Day 10

2020-11-19 - Grid - *Whether you call it a grid or a fishnet, the idea is to visualize something in a grid form*

<img src="figures/day10_grid.png" width="500">

## Day 11

2020-11-11 - 3D - *The magnificent third dimension! Visualize something in 3D*

<img src="figures/day11_3D.gif" width="500">